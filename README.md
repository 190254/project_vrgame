# Gaming with DINO

This is a game about you and your pet dinosaur, 
where you play fetch and also throw zombies 
into a weird ragdoll state with dishes in the kitchen.

The purpose of the project was to experiment with virtual reality, and I have done so
by creating a controller where one can throw and interact with objects and also the scale 
of the environment within the game. It is not the most polished experience, but it experiments 
with the capabilities of the Oculus headets quite well. 

Requirements:

* Any Oculus VR Headset.
* Windows Platform. 
* Oculus Link with Desktop.

Deployment:

* In Unity, go to file > build settings.
* Create Windows Build (80_x64) and choose a location to build the game to.
* Launch the game from within the folder and enjoy!

Alternatively, a .exe installer can be found here : https://drive.google.com/file/d/1lAUdKoUtNhtWcItDH5DCHZmCiFFi-uM6/view?usp=sharing

