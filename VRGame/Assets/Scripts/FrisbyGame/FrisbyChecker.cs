using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrisbyChecker : MonoBehaviour {

    public bool frisbyIn = true;

    private void Start () {
        frisbyIn = true;
    }
    private void OnTriggerExit (Collider other) {
        if (other.gameObject.tag == "Frisby") {
            frisbyIn = false;
        }
    }
    private void OnTriggerEnter (Collider other) {
        if (other.gameObject.tag == "Frisby") {
            frisbyIn = true;
        }
    }
}