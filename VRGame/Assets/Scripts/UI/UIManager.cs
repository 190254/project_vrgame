using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {
    public void LoadSceneFrisby () {
        SceneManager.LoadScene ("DinoFrisby");
    }

    public void LoadSceneZombie () {
        SceneManager.LoadScene ("DinoZombie");
    }
}