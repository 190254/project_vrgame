using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieSpawner : MonoBehaviour {

    public List<GameObject> zombies = new List<GameObject> ();
    public int zombieCount;
    [SerializeField] GameObject zombiePrefab;
    GameObject currentZombie;

    private void Start () {

        StartCoroutine (SpawnZombieC ());

    }

    void SpawnZombie () {
        if (zombieCount < 1) {
            GameObject zombie = Instantiate (zombiePrefab, transform.position, Quaternion.identity);
            zombieCount++;

            currentZombie = zombie;

            zombies.Add (currentZombie);

        }
    }

    private void Update () {

        zombieCount = zombies.Count;

        if (zombieCount < 1) {
            SpawnZombie ();
        }
    }

    IEnumerator SpawnZombieC () {

        while (zombieCount < 1) {
            SpawnZombie ();
        }

        yield return null;
    }
}