using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Zombie : MonoBehaviour {

    [Header ("General")]
    [SerializeField] float speed;
    public bool hasDied = false;

    Waypoint waypoint;
    [SerializeField] int index;

    float deathTimer = 5f;

    bool canWalk = true;

    [Header ("States")]

    [SerializeField] Transform player;
    [SerializeField] float attackCD = 5f;
    bool inRange = false;

    [Header ("Ref")]

    public string wayPointString = "Waypoints";
    public string playerTag = "Player";

    private void Awake () {
        waypoint = GameObject.FindGameObjectWithTag (wayPointString).GetComponent<Waypoint> ();
        player = GameObject.FindGameObjectWithTag (playerTag).transform;

    }

    private void Update () {

        if (index >= waypoint.wayPoints.Length) {
            // Reach final location .. enter attack state
            canWalk = false;
            AttackPlayer ();
        }

        if (canWalk) MoveToWaypont ();

        if (hasDied) {
            deathTimer -= Time.deltaTime;
            if (deathTimer <= 0) {
                Destroy (this.gameObject);
            }
        }

    }

    void MoveToWaypont () {
        transform.position = Vector3.MoveTowards (transform.position, waypoint.wayPoints[index].position, speed * Time.deltaTime);

        transform.LookAt (waypoint.wayPoints[index].position);

        if (Vector3.Distance (transform.position, waypoint.wayPoints[index].position) < 0.4f) {
            index++;

        }
    }

    void AttackPlayer () {

        float distanceToPlayer = Vector3.Distance (transform.position, player.position);

        if (distanceToPlayer < 1) {
            inRange = true;

            if (inRange) {
                // Attack();
                Debug.Log ("has attacked");
            }
        } else {
            transform.position = Vector3.MoveTowards (transform.position, player.position, speed * Time.deltaTime);
        }
    }

    void Attack () {
        // Set Animator 
        // Deal damage to player
    }

    private void OnCollisionEnter (Collision other) {
        if (other.collider.tag == "Dish") {

            // Dish has collided with zombie
            // Zombie dies

            Animator zombieAnim = GetComponent<Animator> ();
            zombieAnim.enabled = false;

            hasDied = true;

        }
    }

}