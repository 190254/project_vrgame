using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class HandPresence : MonoBehaviour {

    InputDevice targetDevice;
    public GameObject handModelPrefab;
    GameObject spawnedHand;

    Animator animator;

    private void Start () {
        GetDevices ();
    }

    void GetDevices () {
        List<InputDevice> devices = new List<InputDevice> ();
        InputDeviceCharacteristics rightControllerChar = InputDeviceCharacteristics.Right;
        InputDevices.GetDevicesWithCharacteristics (rightControllerChar, devices);

        foreach (var item in devices) {
            Debug.Log (item.name + item.characteristics);
        }

        if (devices.Count > 0) {
            targetDevice = devices[0];

        }

        spawnedHand = Instantiate (handModelPrefab, transform);
        animator = spawnedHand.GetComponent<Animator> ();

        Debug.Log (animator);

    }

    void AnimateHand () {
        if (targetDevice.TryGetFeatureValue (CommonUsages.trigger, out float triggerValue)) {
            animator.SetFloat ("Trigger", triggerValue);

        } else {
            animator.SetFloat ("Trigger", 0);
        }

        if (targetDevice.TryGetFeatureValue (CommonUsages.grip, out float gripValue)) {
            animator.SetFloat ("Grip", gripValue);
        } else {
            animator.SetFloat ("Grip", 0);
        }
    }

    private void Update () {
        AnimateHand ();

    }
}