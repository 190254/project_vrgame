using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class InputActionEvent : MonoBehaviour {

    [SerializeField] InputActionReference InputActionReference;
    [SerializeField] UnityEvent<InputAction.CallbackContext> onActionPerformed = new UnityEvent<InputAction.CallbackContext> ();
    [SerializeField] UnityEvent<InputAction.CallbackContext> onActionStarted = new UnityEvent<InputAction.CallbackContext> ();
    [SerializeField] UnityEvent<InputAction.CallbackContext> onActionCanceled = new UnityEvent<InputAction.CallbackContext> ();

    private void OnEnable () {
        InputActionReference.action.Enable ();

        InputActionReference.action.performed += x => onActionPerformed.Invoke (x);
        InputActionReference.action.started += x => onActionStarted.Invoke (x);
        InputActionReference.action.canceled += x => onActionCanceled.Invoke (x);
    }

    private void OnDisable () {
        InputActionReference.action.Dispose ();
    }
}