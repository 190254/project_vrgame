using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XRTeleport : Singleton<XRTeleport> {

    [SerializeField] LayerMask rayLayer;
    [SerializeField] Transform teleportVis;

    Vector3 hitpoint;

    // On release of input

    public void CastRay (Ray ray) {

        if (Physics.Raycast (ray, out RaycastHit hit, 100, rayLayer, QueryTriggerInteraction.Ignore)) {
            teleportVis.gameObject.SetActive (true);
            hitpoint = hit.point;
            teleportVis.position = hitpoint;
        }

    }

    public void Teleport (Transform rig) {
        StartCoroutine (Teleporting (rig));
    }

    IEnumerator Teleporting (Transform rig) {
        float progress = 0;
        Vector3 startPos = rig.position;
        teleportVis.gameObject.SetActive (false);

        while (progress < 1) {
            progress += Time.deltaTime * 10;
            rig.position = Vector3.Lerp (startPos, hitpoint, progress);

            yield return null;
        }

    }

}