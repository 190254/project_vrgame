using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DishChecker : MonoBehaviour {
    private void OnTriggerEnter (Collider other) {
        if (other.TryGetComponent<Dishes> (out Dishes dish)) {
            dish.isInKitchen = true;
        }
    }

    private void OnTriggerExit (Collider other) {
        if (other.TryGetComponent<Dishes> (out Dishes dish)) {
            dish.isInKitchen = false;
        }
    }
}