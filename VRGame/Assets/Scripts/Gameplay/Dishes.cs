using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dishes : MonoBehaviour {
    [SerializeField] bool cleaned = false;

    public bool isInKitchen = true;

    [Header ("Ref")]
    Wolf dinoPet;

    public LayerMask dishLayer;

    private void Awake () {
        dinoPet = GameObject.FindObjectOfType<Wolf> ();
        isInKitchen = true;
    }

    private void Update () {

        if (!isInKitchen) {
            dinoPet.bestTarget = transform;
            //   dinoPet.playerInSightRange = true;
            CheckDinoDistance ();
        }

    }

    private void OnTriggerEnter (Collider other) {
        if (other.tag == "Zombie") {
            Animator zomAnim = other.GetComponent<Animator> ();
            zomAnim.enabled = false;

            Zombie zombie = other.GetComponent<Zombie> ();
            zombie.hasDied = true;

            ZombieSpawner spawner = GameObject.FindGameObjectWithTag ("ZSpawner").GetComponent<ZombieSpawner> ();
            spawner.zombieCount -= 1;
            spawner.zombies.Remove (zombie.gameObject);
        }

    }

    void CheckDinoDistance () {

        float distanceToDino = Vector3.Distance (transform.position, dinoPet.gameObject.transform.position);

        if (distanceToDino <= 0.5f) {
            dinoPet.playerInAttackRange = true;
        }
    }
}