using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Dino : MonoBehaviour {

    [Header ("Ref")]
    [SerializeField] NavMeshAgent _nav;
    //  public Transform player;

    public LayerMask whatIsGround, whatIsDish;

    public GameObject frisby;
    public FrisbyChecker frisbyChecker;

    [Header ("Patrol")]
    // Patrol

    public Vector3 walkPoint;
    bool walkPointSet;
    [SerializeField] float walkPointRange;

    [SerializeField] float searchTimer = 3f;

    [Header ("frisby")]

    // Picking up dish
    [SerializeField] Transform dishPos;

    [SerializeField] Transform playerPos;

    public Transform bestTarget;
    Transform[] currentDishes;
    bool hasFrisby = false;

    [Header ("StateControl")]

    //States

    public float sightRange, attackRange;

    [SerializeField] Transform attackPosition;
    public bool playerInSightRange, playerInAttackRange;

    [Header ("Stats")]

    [SerializeField] float wolfSpeed = 5;

    private void Awake () {
        //        player = GameObject.Find ("Player").transform;
        _nav = GetComponent<NavMeshAgent> ();

    }

    private void Update () {

        //Check for sight and attack
        // playerInSightRange = Physics.CheckSphere (transform.position, sightRange, whatIsDish);
        //   playerInAttackRange = Physics.CheckSphere (attackPosition.transform.position, attackRange, whatIsDish);

        if (!playerInSightRange && !playerInAttackRange) {
            Patrol ();
            // tutAnim.SetBool ("playerInRange", false);
        };
        if (!frisbyChecker.frisbyIn) {
            //tutAnim.SetBool ("playerInRange", true);
            Chase ();
        }

        float distanceToFrisby = Vector3.Distance (transform.position, frisby.transform.position);

        if (distanceToFrisby <= 1f && !frisbyChecker.frisbyIn) {

            PickupFrisby ();
        }

        // DoWolfAnim ();

    }

    //! Wolf Logic

    //? Movement

    void Patrol () {
        if (!walkPointSet) SearchWalkPoint ();

        if (walkPointSet) _nav.SetDestination (walkPoint);

        Vector3 distanceToWalkpoint = transform.position - walkPoint;

        // Walkpoint reached 
        if (distanceToWalkpoint.magnitude < 1f) walkPointSet = false;
    }
    void SearchWalkPoint () {

        _nav.speed = 0;

        searchTimer -= Time.deltaTime;

        if (searchTimer <= 0) {

            _nav.speed = 1;

            searchTimer = 0;

            float randomZ = Random.Range (-walkPointRange, walkPointRange);
            float randomX = Random.Range (-walkPointRange, walkPointRange);

            walkPoint = new Vector3 (transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

            if (Physics.Raycast (walkPoint, -transform.up, 2f, whatIsGround))
                walkPointSet = true;
            searchTimer = 3f;

        }

    }
    void Chase () {

        // Look for closest dish 
        //playerInSightRange = true;

        _nav.SetDestination (frisby.transform.position);

    }

    void PickupFrisby () {

        frisby.transform.position = dishPos.position;
        hasFrisby = true;
        // Pick dish up 
        // Dish transform = new transform that follows raptor 
        // Play dish pickup animator
        //  transform.LookAt (player);

        if (hasFrisby) {
            // Move to player location and drop dish near player
            // If has reached location then drop dish and resume patrolling 
            _nav.SetDestination (playerPos.position);

            float distanceToPlayer = Vector3.Distance (transform.position, playerPos.position);

            if (distanceToPlayer <= 0.5f && frisbyChecker.frisbyIn) {
                hasFrisby = false;

                //  playerInAttackRange = false;
                //  playerInSightRange = false;
                //Patrol ();

            }

        }

    }

}