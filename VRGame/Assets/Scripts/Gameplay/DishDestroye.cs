using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DishDestroye : MonoBehaviour {
    private void OnTriggerExit (Collider other) {
        if (other.TryGetComponent<Dishes> (out Dishes dish)) {
            Destroy (dish);
        }
    }
}