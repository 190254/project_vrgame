using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateSpawner : MonoBehaviour {

    [SerializeField] List<GameObject> possibleDishes = new List<GameObject> ();

    [SerializeField] GameObject currentObject;

    int plateCount = 0;

    private void Update () {
        SpawnPlate ();

    }

    void SpawnPlate () {
        if (plateCount < 1) {

            int dishInt = Random.Range (0, possibleDishes.Count);
            GameObject dish = Instantiate (possibleDishes[dishInt], transform.position, Quaternion.identity);

            plateCount = 1;

            currentObject = dish;

        }

        Dishes dishScript = currentObject.GetComponent<Dishes> ();
        if (!dishScript.isInKitchen) {
            plateCount -= 1;

            if (plateCount <= 0) {
                plateCount = 0;
            }
        }
    }

}